# == Schema Information
#
# Table name: short_urls
#
#  id         :integer          not null, primary key
#  url        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  unique_id  :string           not null
#
# Indexes
#
#  index_short_urls_on_unique_id  (unique_id) UNIQUE
#
class ShortUrlSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :url, :unique_id, :links, :visited

  def links
    route_options = { host: instance_options[:host_with_port] }

    {
      visit: visit_short_url_url(object.unique_id, route_options)
    }
  end

  def visited
    object.visits.count
  end
end
