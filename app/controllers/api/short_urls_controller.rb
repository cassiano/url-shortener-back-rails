class Api::ShortUrlsController < Api::ApiController
  before_action :find_current_short_url, only: [:show, :destroy]

  def index
    render default_serializer_options.merge(json: scoped_short_urls.order(updated_at: :desc))
  end

  def create
    short_url = ShortUrl.new(create_params)

    if short_url.save
      session[:short_urls] << short_url.id

      render default_serializer_options.merge(json: short_url, status: :created)
    else
      render json: short_url.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @short_url.destroy

    head :no_content  # 204 = No Content
  end

  def show
    render default_serializer_options.merge(json: @short_url)
  end

  def visit
    short_url = ShortUrl.find_by!(unique_id: params[:unique_id])

    short_url.visits.create user_agent: request.user_agent, ip: request.ip

    redirect_to short_url.url, allow_other_host: true
  end

  def stats
    render json: to_camel_lower(
      short_urls: {
        generated: ShortUrl.count,
        visited: ShortUrlVisit.count
      }
    )
  end

  private

  def create_params
    params.permit :url
  end

  def find_current_short_url
    @short_url = scoped_short_urls.find(params[:id])
  end

  def scoped_short_urls
    ShortUrl.where id: session[:short_urls]
  end
end
