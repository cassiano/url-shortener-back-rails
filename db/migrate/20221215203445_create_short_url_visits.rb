class CreateShortUrlVisits < ActiveRecord::Migration[7.0]
  def change
    create_table :short_url_visits do |t|
      t.references :short_url, null: false, foreign_key: { on_delete: :cascade }
      t.string :user_agent, null: false
      t.string :ip, null: false

      t.timestamps
    end
  end
end
