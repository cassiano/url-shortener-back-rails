# == Schema Information
#
# Table name: short_urls
#
#  id         :integer          not null, primary key
#  url        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  unique_id  :string           not null
#
# Indexes
#
#  index_short_urls_on_unique_id  (unique_id) UNIQUE
#
class ShortUrl < ApplicationRecord
  validates :url, presence: true, url: true
  validates :unique_id, presence: true, uniqueness: true

  has_many :visits, class_name: 'ShortUrlVisit'

  before_validation :generate_unique_id

  private

  def generate_unique_id
    self.unique_id ||= SecureRandom.uuid
  end
end
