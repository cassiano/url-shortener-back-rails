# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

# Defines the root path route ("/")
# root "articles#index"
Rails.application.routes.draw do
  namespace :api do
    resources :short_urls, only: [:index, :destroy, :create, :show] do
      get :stats, on: :collection
    end
  end

  get '/short_urls/:unique_id/visit' => 'api/short_urls#visit', as: :visit_short_url
end
