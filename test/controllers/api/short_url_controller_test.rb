require "test_helper"

class Api::ShortUrlControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_short_url_index_url
    assert_response :success
  end

  test "should get destroy" do
    get api_short_url_destroy_url
    assert_response :success
  end

  test "should get show" do
    get api_short_url_show_url
    assert_response :success
  end

  test "should get create" do
    get api_short_url_create_url
    assert_response :success
  end
end
