class Api::ApiController < ApplicationController
  before_action :snake_case_params
  before_action :set_session_short_urls_default

  private

  def snake_case_params
    request.parameters.deep_transform_keys! &:underscore
  end

  def default_serializer_options
    { host_with_port: request.host_with_port }
  end

  def to_camel_lower(hash)
    hash.deep_transform_keys { |key| key.to_s.camelize.sub(/^./, &:downcase) }
  end

  def set_session_short_urls_default
    session[:short_urls] ||= []
  end
end
