class CreateShortUrls < ActiveRecord::Migration[7.0]
  def change
    create_table :short_urls do |t|
      t.string :unique_id, null: false, index: { unique: true }
      t.string :url, null: false

      t.timestamps
    end
  end
end
